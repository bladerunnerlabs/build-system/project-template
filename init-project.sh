#!/bin/bash

SCRIPT_PATH=$(readlink -e $0)
SCRIPT_DIR=$(dirname ${SCRIPT_PATH})
SCRIPT_NAME=$(basename ${SCRIPT_PATH})

BUILD_RUNNER="build-runner"
BR_DIR=${SCRIPT_DIR}/${BUILD_RUNNER}

if [[ "${TERM}" =~ "xterm" ]]; then
    RED='\033[0;31m'
    GREEN='\033[0;32m'
    YELLOW='\033[1;33m'
    NC='\033[0m' # No Color
else
    RED=
    GREEN=
    YELLOW=
    NC=
fi

echo_red() {
    echo -e "${RED}$@${NC}"
}

echo_green() {
    echo -e "${GREEN}$@${NC}"
}

echo_yellow() {
    echo -e ${YELLOW}$@${NC}
}

function err_exit() {
    echo "$@"
    exit 1
}

manifest_yaml="manifest.yaml"
infra_yaml="infra.yaml"
config_file=".project/config"
config_path="${SCRIPT_DIR}/${config_file}"
build_runner_url=
build_runner_tag=
NO_SYNC=false

function usage() {
    [ -n "$1" ] && echo_red "$1\n"
    echo -e "\nUsage: ${SCRIPT_NAME} [options]"
    echo -e "\t-h, --help : print usage message\n"
    echo -e "Normally ${SCRIPT_NAME} retrieves a bootstrap ${BUILD_RUNNER}, then"
    echo -e "performs 'project git sync' for infra, then for project modules."
    echo -e "See --no-sync for handling special cases.\n"
    echo -e "\t-C, --config <file-path> : config file path"
    echo -e "\t\tdefault : ${config_file}"
    echo -e "\t-u, --url <url> : build-runner Git repo URL"
    echo -e "\t\toverrides: build_runner_url in config file"
    echo -e "\t-r, --revision <git-rev> : build-runner Git tag/revision"
    echo -e "\t\toverrides: build_runner_tag in config file\n"
    echo -e "\t-M, --manifest <file-name> : project manifest yaml file path"
    echo -e "\t\tdefault : ${manifest_yaml}"
    echo -e "\t-I, --infra <file-name> : infra manifest yaml file path"
    echo -e "\t\tdefault : ${infra_yaml}\n"
    echo -e "\t-- * : all arguments after -- are passed to Git commands, e.g.:"
    echo -e "\t\t./${SCRIPT_NAME} -r latest -- --progress\n"
    echo -e "\t-N, --no-sync: do not sync infra and project modules"
    echo -e "\tThis may be useful if different extra args (after --)"
    echo -e "\tshould be passed at each sync step, e.g.:"
    echo -e "\t\t./${SCRIPT_NAME}"
    echo -e "\t\t./project git sync -M infra.yaml -- --progress"
    echo -e "\t\t./project git sync -M manifest.yaml -- --depth=1"

    exit 1
}

short_op="u:r:C:M:I:Nh"
long_op="url:,revision:,config:,manifest:,infra:,no-sync,help"

function parse_arguments() {
    while [ $# -gt 0 ]; do
        case $1 in
                (-u | --url) user_build_runner_url=$2; shift ;;
                (-r | --revision) user_build_runner_tag=$2; shift ;;
                (-C | --config) config_path=$2; shift ;;
                (-M | --manifest) manifest_yaml=$2; shift ;;
                (-I | --infra) infra_yaml=$2; shift ;;
                (-N | --no-sync) NO_SYNC=true; ;;
                (-h | --help) usage ;;
                # default options
                (--) shift; break ;; # $@ contains remaining pos args
                (-*) usage "error - unrecognized option $1" ;;
                (*) break ;;
        esac
        shift
    done
}

################ Start ################

# parse arguments 

# find custom arguments, passed after --
args="$@"
normal_args="${args%%-- *}" # all args until --
if [ "${normal_args}" != "${args}" ]; then
    extra_args="${args##*-- }" # all args after --
fi

# parse regular command line arguments
if [ -n "${short_op}" ]; then
    options=$(getopt --options ${short_op} --long ${long_op} -- ${normal_args})
    if [ $? -ne 0 ]; then
        usage 1 "failed to parse: ${normal_args} for ${cmd}"
    fi
else
    options="${normal_args}"
fi

eval set -- ${options} # set new $@

parse_arguments "$@"

# if cmd line did not override all config vars read from config file
if [[ -z "${user_build_runner_url}" || -z "${user_build_runner_tag}" ]]; then
    if [ ! -e "${config_path}" ]; then
        err_exit "project config not found: ${config_path}"
    fi
    CONFIG_PATH=$(readlink -e ${config_path})
    source ${CONFIG_PATH}
    if [[ -z "${build_runner_url}" || -z "${build_runner_tag}" ]]; then
        err_exit "project config should define build_runner_url, build_runner_tag, file: ${config_path}"
    fi
fi

if [ -n "${user_build_runner_url}" ]; then
    build_runner_url=${user_build_runner_url}
fi
if [ -n "${user_build_runner_tag}" ]; then
    build_runner_tag=${user_build_runner_tag}
fi

# make sure basic prerequisites are installed
if ! which git &>/dev/null; then
    err_exit "git not found, please install"
fi

if ! which python3 &>/dev/null; then
    err_exit "python3 not found, please install"
fi

# create temp dir for bootstrap blade-runner
TMP_BR_DIR=$(mktemp -d "${BR_DIR}.${build_runner_tag}.boot-XXXXX")

# clone bootstrap build-runner
echo_yellow "clone bootstrap url: ${build_runner_url}, rev: ${build_runner_tag}"
clone_opts="-q -c advice.detachedHead=false --no-single-branch ${extra_args}"
clone_opts+=" --branch ${build_runner_tag} ${build_runner_url} ${TMP_BR_DIR}"
echo "git clone ${clone_opts}"
git clone ${clone_opts} || err_exit "failed: git clone ${clone_opts}"
echo_green "cloned bootstrap to: ${TMP_BR_DIR}, rev: ${build_runner_tag}\n"

# create symbolic link to bootstrap project
[ ! -e ${TMP_BR_DIR}/project ] && err_exit "${TMP_BR_DIR}/project not found after clone"
ln -snf ${TMP_BR_DIR}/project ${SCRIPT_DIR}/project

# git sync the infra modules
sync_opts="-M ${infra_yaml} -I"
if [ -n "${extra_args}" ]; then
    sync_opts+=" -- ${extra_args}"
fi
echo_yellow "./project git sync ${sync_opts}"
${SCRIPT_DIR}/project git sync ${sync_opts} || err_exit "failed:project git sync ${sync_opts}"
echo_green "project infra updated OK\n"

# create symbolic link to real project
[ ! -e ${BR_DIR}/project ] && err_exit "${BR_DIR}/project not found after update"
ln -snf ${BR_DIR}/project ${SCRIPT_DIR}/project

# clean up the backup dir
if [ -d ${TMP_BR_DIR} ]; then
    echo_green "remove bootstrap: ${TMP_BR_DIR}"
    rm -rf ${TMP_BR_DIR}
fi

[ "${NO_SYNC}" == "true" ] && { echo_yellow "project git sync skipped"; exit 0; }

# git sync the project modules
sync_opts="-M ${manifest_yaml} -I"
if [ -n "${extra_args}" ]; then
    sync_opts+=" -- ${extra_args}"
fi
echo_yellow "./project git sync ${sync_opts}"
${SCRIPT_DIR}/project git sync ${sync_opts} || err_exit "failed:project git sync ${sync_opts}"

# project-template
This is a template repository for projects based on **build-runner** and **cmake-runner**.

[[_TOC_]]

## Starting a new project

- Clone this repository.
    - It's a good idea to name the target directory using the actual name of the new project.
- Bootstrap the project:
    - the template repository has `infra.yaml` populated with the latest infrastructure modules necessary for its own functioning; these are usually `build-runner`, `cmake-runner` and `pack-runner` for C/C++ projects; the bare minimum is `build-runner`, which may be the right choice e.g. for projects that utilize Git functionality only.
    - running the script `bootstrap.sh` clones these modules, performs initial directory setup, removes itself and saves the new state as a git commit.
    - the original branch is named `template`, the bootstrap changes are committed to a new `master` branch;
    - it is recommended to keep the `template` branch as it might be used for fixes and updates of the "bootstrap-level" files.
- From now on the project is managed by the script named `project`
    - it uses `build-runner` code to manage multi-repository projects
- Populate `manifest.yaml` with the modules relevant for the new project.
    - the original `manifest.yaml` provides only a template following the supported format.
    - create a new manifest by setting all required fields in the yaml file and add new modules
- Fetch the modules using `project git sync` and check that everything is right.
- Push the project to its own remote git repository
    - create a new git commit reflecting the first effective version of the project `manifest.yaml`
    - add a new `git remote` and push the repository there;
      - template repository has been renamed to `template` during bootstrap;
      - so the project repository may be named `origin` or anything else.
    - push the new version, e.g.: `git push origin master`
- When the new project is cloned, run `init-project.sh` script
    - `init-project.sh` pulls both the infrastructure and project modules according to the manifest.
    - it was already called by `bootstrap.sh`, so no need to call now locally
    - infrastructure modules can be always updated by using `init-project.sh` script again.
   

For example:
```
git clone https://gitlab.com/bladerunnerlabs/build-system/project-template.git my-project
cd my-project
./bootstrap.sh
vim manifest.yaml # add modules
./project git sync
git commit -a -m "manifest populated"
git remote add origin ${my-project-url}
git push origin --all
```

## Starting your work with the project

### Cloning and setting up
Clone this git repository and switch to the relevant branch.

Then run `init-project.sh` to bootstrap the infrastructure and pull all modules defined by the manifest. For example:
```
git clone git@gitlab.com:xyz/abc_proj.git
cd abc_proj
./init-project.sh
```
Output may look like this:
```
$ ./init-project.sh
. . .

```

Note that `./init-project.sh` should be normally called only **once**, after the repository is cloned. It pulls a stable version of [`build-runner`](https://gitlab.com/bladerunnerlabs/build-system/build-runner), creates `./project` symbolic link, then pulls the infrastructure and project modules.

After that all operations should be initiated using `./project`. Note that `./init-project.sh` may still be used as a resque step if, for example, `blade-runner` directory was lost.

Please refer to `./project` help available on command line and in the documentation for [`build-runner`](https://gitlab.com/bladerunnerlabs/build-system/build-runner).

### Autocompletion for project tool
Please note that autocompletion feature is available for `./project`. To enable it the following steps may be performed:
```
./project autocomp > /tmp/auto.sh
source /tmp/auto.sh
```
A similar procedure may be coded into a `.bashrc` file to enable `./project` autocompletions automatically on startup.

### Manifest
All aspects of work with the `./project` tool ultimately depend on a manifest file, usually named `manifest.yaml`. Its format specification may be found in [`manifest file documentation of build-runner`](https://gitlab.com/bladerunnerlabs/build-system/build-runner#manifest-file-format).

The manifest file defines a list of modules which comprise the project.

For example, this is a fragment of a possible `mfc` module definition:

```
modules:
    module_a:
        url: git@gitlab.com:xyz/module_a.git
        revision: master
        platform: cmake-runner
        pre-build:
            - module_a/install.sh
        depends:
            - module_b
            - module_c
```

### Retrieving the modules

The source trees for all modules can be retrieved using:
```
./project git sync
```

A single module or a list of modules may be synchronized too, for example:
```
./project git sync module_a module_c
```

### Building the project

The project is supposed to be built either module by module or as a single tree. 

#### module build
This is achieved by invoking:
```
./project module build
```
This build function shall build all modules in parallel while respecting dependencies between the modules. For example, in the manifest fragment above, `module_a` module will be submitted for build only after all modules it depends upon (`module_b`, `module_c`) have been built successfully.

#### tree build
or: 
```
./project tree build
```

#### Specifying custom modules list
A single module or a list of modules may be requested. For example:
```
./project module build module_b
./project tree build module_a module_c
```

#### Specifying build target
When no target is specified, all 'leaf' modules (those upon which no other modules depend) are built with `all` target, all `non-leaf` ones are built with `install` target. 

It is possible to specify a certain target, using `--target` or `-t` option. For example:
```
./project module build mfc -t install
```

#### Skipping dependencies
It is possible to skip building dependencies by using `--nodeps` or `-N`.

If the manifest defines a `pre-build` step for a module, it may be skipped by using `--noprebuild` or `-P`.

For example:
```
./project module build mfc -N -P
```
combines both options and starts build of `mfc` without building other modules it depends upon and without executing its pre-build script. This may be useful when all dependencies have been built and installed previously and skipping them may save time without compromising correctness. 

#### Troubleshooting project build
If porject build fails, you can try purging previous build:
```bash
./project module purge -b
```

#### Showing temp Makefile
The exact commands used to build and install various modules and the dependencies between them are coded into a temporary Makefile created on the fly by `./project`. To see the Makefile for any subcommand use `-m` or `--makefile`. This may be useful for debugging, e.g. for reproducing the individual build steps manually. For example, in case of the overall build step:
```
./project module build --makefile
```

## Using CLion
The following symbols and options should be defined in: `Settings` --> `Build,Execution,Deployment` --> `CMake` --> `Debug`/`Release` --> `CMake options`

### example project in Debug mode
```
-G "Ninja" -DCMAKE_RUNNER_NOCOLOR:BOOL=YES -DCMAKE_BUILD_TYPE=Debug -DCMAKE_RUNNER_DIR=/home/$USER/my_project/cmake-runner
```

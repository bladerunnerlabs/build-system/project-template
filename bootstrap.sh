#!/bin/bash

boot_script="$(basename $0)"
git_remote_name="project-template"
project_script="build-runner/project"
project_link="project"

function err_exit() {
    echo "$@"
    exit 1
}

echo -e "Setup the project infrastruture...\n"
./init-project.sh --no-sync || err_exit "init-project.sh failed"

if [ ! -e ${project_script} ]; then
    err_exit "no ${project_script} after init"
fi

origin_remote=$(git remote | grep "^origin$")
remote_nocolor=$(echo "${origin_remote}" | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g")
if [ "${remote_nocolor}" == "origin" ]; then
    echo -e "\nRename git remote to '${git_remote_name}'..."
    git remote rename origin ${git_remote_name} || echo "failed to rename remote 'origin'"
else
    echo -e "\nremote 'origin' not found, renaming to '${git_remote_name}' skipped..."
fi

echo -e "\nCreate new 'master' branch..."
git show-ref -q master &>/dev/null && git branch -D -q master
git checkout -b master || err_exit "failed to create 'master' branch"

echo -e "\nRemove bootstrap scripts from git..."
git rm -r --cached -q ${boot_script}

echo -e "Commit changes in git..."
git commit -m "project bootstrapped" -q || err_exit "failed to create post-bootstrap git commit"

echo -e "\ngit branches:"
git branch
echo -e "last git commits:"
git log --oneline -n 2